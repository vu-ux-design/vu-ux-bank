# TODO list

1) 
    Create doc 

    The assignment consists of the following parts:
    • title page (not numbered),
    • annotation (not numbered),
    • table of contents (not numbered),
    • problem statement (numbered, 1st assignment),
        - project titles,
        - project description,
        - stakeholders,
    • personas (numbered, 2nd assignment),
    • user needs (numbered, 3rd assignment),
    • user journey map (numbered, 4th assignment),
    • usability testing (numbered, 6th assignment).

2)
    Presentation(?) need or not. Not sure.

2)
    Make mockups, diagrams, mind map(?), etc.

# Doc instructions

Title page contains:
• names of university, faculty and course,
• project full title,
• assignment title,
• team members’ names and surnames,
• year.
**Annotation** contains the summary of the performed work, team member’s contribution and email. This chapter is not numbered and is not included to the table of contents.

**Project titles.** Project must have two titles: full (e.g. "Vilnius University Library App") and short (e.g. "Library"). Full title is used on the title page and in this section, short title serves as a project reference in the document text and included to assignments’ names.

**Problem problem statement** should be short (about a half of page) and specific about the highlevel project goals. The problem should be described in terms of user activities and situations where the problem occurs, and what aspects of the situation might be improved with a technical solution.
Avoid describing or suggesting a solution at this stage that will hamper your design thinking when you actually start solving the problem.
To gather the relevant facts for your problem statement, you can use a simple technique called the 5 Ws, which involves answering the questions below:
1. Who is affected by the problem?
2. What is the problem?
3. Where does this problem occur?
4. When does the problem occur?
5. Why does the problem occur? Why is the problem important?

**Stakeholders.** Specify the specific activity which will be supported by your design project:
• Who is directly involved to the chosen activity? (Primary users)
• Who is occasionally involved? Maybe somebody is involved via somebody else? (Secondary users)
• Maybe somebody will receive the output of your system? (Indirect users, stakeholders)
• You will make purchase decision? Maybe people use competitor’s tools now and you are going to provide them the better solution? (Indirect users, stakeholders)
Answer to these questions in terms of primary, secondary (if exist) and tertiary users. The users and other stakeholder groups: there can be a range of different user groups as well as other stakeholder groups whose needs are important. Relevant groups shall be identified and their relationship with the proposed development described in terms of key goals and constraints.

**Personas and other sections.** The explanation will be provided with specific assignments.

**Section numbering:** Headings of the introduction and the main part sections are numbered strictly hierarchically. The sections have assigned numbers, such as 1., subsections – 1.1., 1.2, subsubsection – 1.1.1., 1.1.2. and so on.
File naming requirements. A file name contains short project title, assignment number and assignment title. Example: *CarBuddy 1 Project proposal.pdf*