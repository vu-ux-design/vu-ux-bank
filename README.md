# Proposed improvememt Swedbank application experience

<p align="center">
**Vilnius University, Faculty of Mathematics and Informatics, 1st year, 2nd semester.**
</p>

<p align="center">
Volodymyr Kadzhaia, Bhumika Shiradanahalli Dhananjaya, David Olurebi.
</p>

## Annotation

Redesign the existing fanctionality of Swedbank's application. The team members in the development of this project are Volodymyr Kadzhaia, David Olurebi, Bhumika Shiradanahalli Dhananjaya. 

[TOCM]

[TOC]

## Introduction

Modern technology has taken banking to a whole new level. Ease, speed, enjoyment, and convenience are rewriting the history of the user experience in banking. While many banks still struggle to digitize their products. 

**Motivation:**
For today’s standards, the UI looked old. There were unwanted functionalities in the application which no one was ever going to use, moreover the functionalities were not categorised. Swedbank app is monolith. There is also a problem that the application functions are not implemented within the application and the redirection to webpage does not deliver the sense of using a mobile application.

## Problem statement

The main problem of all banks are the user experience is too complex, making navigation frustrating. There are several problems in all bank systems:

#### Authorization

Recently many applications have focused on simplifying authorization. The security of financial applications is very important for users, but a long and complicated login shows down the work with the application. There needs to be a balance between security and covenience but the client's interests must come first. Difficult to entry into the application should not create obstacles for the user. SMS activation, pin-calculator, confirmations, dependence on third-party services, complex activation leads to the fact that the user, in order to perform a simple action, spends more time logging in.

### Title screen

The first impression of the application is very important. The user should immediately understand what can be done from the main screen. Swedbank application focus on account balance (yes, this is important) but it is equally important to give the user a quick opportunity to implement the required actions, and not to play a quest for finding the reight service and functionality. The main features of the Swedbank's application are hidden, which does not allow the user to fully work with the app.

<!-- ### Service 

One of the problem of many banks is that they do not provide services from third-party developers. In this way, it will be easier and faster for users to order the food, taxis, and more, and for banks to expand their user base. -->

We analyzed many features, but we also need something more emotional. Something that will make customers love our banking application, inspire them, and enable them to truly enjoy and benefit from the experience. 

## Stakeholders

Stakeholders: As a design project aimed at the banking and financial tech industry. The major stakeholder of this deliverable are those individuals(bank customers) who have adopted the digital means to utilize banking services through the bank's application.

One of the elementary goals of technology is to avoid repetition by the reusing of existing data. Thus, entities such as Sodra, VMI, etc. are, upon access being granted by the user, able to make use of an individual's information to fill form and documents. Therefore, governmental entities are considered as the secondary users of the application.

## Personas

## User needs

## User journey map

## Usability testing

